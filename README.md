# Geotagged2KML

```
usage: geotag2kml.py [-h] [--kmz KMZ] img kml

Generates KML/KMZ from a directory of geotagged images

positional arguments:
  img         Directory with geotagged images
  kml         Directory for generated KML

optional arguments:
  -h, --help  show this help message and exit
  --kmz KMZ   Path for generated KMZ (.kmz)
```