import argparse
import os
import math
import shutil
import zipfile
import exifread
import random

#some code adapted from https://developers.google.com/kml/articles/geotagsimple

#some constants
ALLOWED_EXT = [".jpg",".jpeg"]
IMG_OUTPUT_DIR = "images"

def list_dir(path):
   p = os.path.abspath(path)
   if os.path.exists(p)==False:
      print "[NOT FOUND] %s"%p
      return []
   print "Finding images..."
   files = []
   for l in os.listdir(p):
      ext = os.path.splitext(l)
      if len(ext)!=2:continue
      ext = ext[1]
      if len(ext)==0:continue
      if ext in ALLOWED_EXT:
         files.append(l)
   return files

def read_tags(path):
   if os.path.exists(path)==False:
      print "[NOT FOUND] %s"%path
      return None
   f = open(path,"rb")
   tags = exifread.process_file(f)
   f.close()
   return tags

def get_size(tags):
   if 'EXIF ExifImageLength' not in tags: return None
   if 'EXIF ExifImageWidth' not in tags: return None
   l = tags['EXIF ExifImageLength'].values
   if len(l)!=1:return None
   w = tags['EXIF ExifImageWidth'].values
   if len(w)!=1:return None
   return (w[0],l[0])

def get_location(tags):
   if 'GPS GPSLatitude' not in tags:return None
   if 'GPS GPSLongitude' not in tags: return None
   if 'GPS GPSLatitudeRef' not in tags: return None
   if 'GPS GPSLongitudeRef' not in tags: return NOne
   lat_dms = tags['GPS GPSLatitude'].values
   long_dms = tags['GPS GPSLongitude'].values
   latitude = dms_to_decimal(lat_dms[0].num, lat_dms[0].den, lat_dms[1].num, lat_dms[1].den, lat_dms[2].num, lat_dms[2].den)
   longitude = dms_to_decimal(long_dms[0].num, long_dms[0].den, long_dms[1].num, long_dms[1].den, long_dms[2].num, long_dms[2].den)
   if tags['GPS GPSLatitudeRef'].printable == 'S': latitude=latitude*(-1)
   if tags['GPS GPSLongitudeRef'].printable == 'W': longitude=longitude*(-1)
   altitude = 0
   if 'GPS GPSAltitude' in tags and 'GPS GPSAltitudeRef' in tags:
      alt = tags['GPS GPSAltitude'].values[0]
      if alt.den!=0: 
         altitude = alt.num/alt.den
         if tags['GPS GPSAltitudeRef']==1: altitude=altitude*-1
   return (latitude,longitude,altitude)

def dms_to_decimal(degree_num, degree_den, minute_num, minute_den, second_num, second_den):
  degree = float(degree_num)/float(degree_den)
  minute = float(minute_num)/float(minute_den)/60
  second = float(second_num)/float(second_den)/3600
  return degree + minute + second

def gen_kml_dir(kml_dir,img_dir,img_scale=1):
   if os.path.exists(kml_dir):
      print "KML directory already exists"
      return
   kml_dir = os.path.abspath(kml_dir)
   kml_images_dir = os.path.join(kml_dir,IMG_OUTPUT_DIR)
   os.makedirs(kml_images_dir)
   files = list_dir(img_dir)
   placemarks = []
   print "Generating KML..."
   for f in files:
      p = gen_placemark(img_dir,f,img_scale)
      if len(p)>0:
         img_src = os.path.abspath(os.path.join(img_dir,f))
         img_dst = os.path.join(kml_images_dir,f)
         shutil.copy(img_src,img_dst)
         placemarks.append(p)
   kml = gen_kml(placemarks)
   kml_name = "%s.kml"%os.path.split(kml_dir)[1]
   kml_path = os.path.join(kml_dir,kml_name)
   print "Wirting KML [%s]"%kml_path
   f = open(kml_path,"wb")
   f.write(kml)
   f.close()
   
def gen_kml(placemarks):
   l='<?xml version="1.0" encoding="UTF-8"?>'+os.linesep
   l=l+'<kml xmlns="http://www.opengis.net/kml/2.2">'+os.linesep
   l=l+'<Document>'+os.linesep
   for placemark in placemarks:
      if len(placemark)>0: l=l+placemark
   l=l+'</Document>'+os.linesep
   l=l+'</kml>'+os.linesep
   return l

def gen_placemark(img_dir,img_filename,img_scale=1):
   img_path = os.path.join(img_dir,img_filename)
   if os.path.exists(img_path)==False:
      print "[NOT FOUND] %s"%path
      return ""
   tags = read_tags(img_path)
   location = get_location(tags)
   img_width = ""
   img_height = ""
   img_size = get_size(tags)
   if img_scale>1: img_scale=1
   if img_scale<=0:img_scale=1
   if img_scale!=1:
      img_width = "width=%dpx"%(math.floor(img_size[0]*img_scale))
      img_height= "height=%dpx"%(math.floor(img_size[1]*img_scale))
   if location is None: return ""
   l = '<Placemark>'+os.linesep
   l = l+'<name>%s</name>%s'%(img_filename,os.linesep)
   l = l+'<description><![CDATA[<img %s %s src=\"%s\" />]]></description>%s'%(img_width,img_height,os.path.join(IMG_OUTPUT_DIR,img_filename),os.linesep)
   l = l+'<Point><coordinates>%.10f,%.10f,%.10f</coordinates></Point>%s'%(location[1],location[0],location[2],os.linesep)
   l = l+'</Placemark>'+os.linesep
   return l

def make_kmz(kml_dir,kmz_path):
   print "Generating KMZ..."
   r = random.randint(0,0xffff)
   zip_filename = "tmp_%d_%s"%(r,os.path.splitext(os.path.basename(kmz_path))[0])
   base_name = os.path.join(os.path.split(kmz_path)[0],zip_filename)
   root_dir = kml_dir
   base_dir = "."
   if os.path.exists(root_dir)==False: os.makedirs(root_dir)
   shutil.make_archive(base_name, "zip", root_dir, base_dir)
   zip_path = base_name+".zip"
   shutil.move(zip_path,kmz_path)

description = "Generates KML/KMZ from a directory of geotagged images"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('img', help='Directory with geotagged images')
parser.add_argument('kml', help='Directory for generated KML')
parser.add_argument('--kmz', help='Path for generated KMZ (.kmz)')
parser.add_argument('--scale', type=float, help='Scale (0..1) images for KML (Default: 1)')
args = parser.parse_args()

img_dir = ""
kml_dir = ""
kmz_file = ""
scale = 1
init_err = False
if args.kmz is not None:
   ext = os.path.splitext(args.kmz)
   kmz_file = ext[0]+".kmz"
   if os.path.exists(kmz_file)==True:
      print "KMZ file already exists. Not overwriting. [%s]"%kmz_file
      init_err=True
   kmz_file = os.path.abspath(kmz_file)
if os.path.exists(args.img)==False:
   print "Image directory does not exists. [%s]"%args.img
   init_err = True
if os.path.exists(args.kml)==True:
   print "KML directory already exists. Not overwriting. [%s]"%args.kml
   init_err=True
if args.scale is None: scale=1
else:
   if args.scale==0: args.scale=1
   if args.scale<0 or args.scale>1: 
      print "Scale value must be between 0 and 1"
      init_err = True
   else: scale=args.scale
if init_err==False:
   img_dir = os.path.abspath(args.img)
   kml_dir = os.path.abspath(args.kml)
   print "Image Directory: %s"%img_dir
   print "KML Directory: %s"%kml_dir
   if args.kmz: print "Generate KMZ: %s"%kmz_file
   else: print "Generate KMZ: False"
   gen_kml_dir(kml_dir,img_dir,img_scale=scale)
   if len(kmz_file)>0: make_kmz(kml_dir,kmz_file)